import Sidebar from './SideBar.vue';
import SidebarItem from './SidebarItem.vue';

const SidebarStore = {
  showSidebar: false,
  sidebarLinks: [],
  isMinimized: false,
  displaySidebar(value) {
    this.showSidebar = value;
  },
  toggleMinimize() {
    document.body.classList.toggle('sidebar-mini');
    // we simulate the window Resize so the charts will get updated in realtime.
    const simulateWindowResize = setInterval(() => {
      window.dispatchEvent(new Event('resize'));
    }, 180);

    // we stop the simulation of Window Resize after the animations are completed
    setTimeout(() => {
      clearInterval(simulateWindowResize);
    }, 1000);

    this.isMinimized = !this.isMinimized;
    document.cookie = 'isMinimized=' + this.isMinimized; //Cookieに保存
  },
  load_cookie() {
    var arr = document.cookie.split(';');

    for (let value of arr) {
      //cookie名と値に分ける
      var content = value.split('=');
      if (content[0] == 'isMinimized') {
        if (content[1] == 'false') {
          this.isMinimized = false;
        } else {
          this.isMinimized = true;
        }

        if (this.isMinimized == true) {
          document.body.classList.toggle('sidebar-mini');
        }
      }
    }
  },
};

const SidebarPlugin = {
  install(app, options) {
    if (options && options.sidebarLinks) {
      SidebarStore.sidebarLinks = options.sidebarLinks;
    }
    app.mixin({
      data() {
        return {
          sidebarStore: SidebarStore,
        };
      },
    });

    Object.defineProperty(app.config.globalProperties, '$sidebar', {
      get() {
        return SidebarStore;
      },
    });
    app.component('side-bar', Sidebar);
    app.component('sidebar-item', SidebarItem);
  },
};

export default SidebarPlugin;
