import 'es6-promise/auto';
import { createApp, computed } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
// import VueMaterial from 'vue-material';

import i18n from '@/i18n';

import InfiniteLoading from 'vue-infinite-loading';
import { ModelListSelect } from 'vue-search-select';
import VueTagsInput from '@johmun/vue-tags-input';
import moment from 'moment';
import QrcodeVue from 'qrcode.vue';
import HeatmapVideo from './views/layouts/HeatmapVideo.vue';
import VRVideoPlayer from './views/layouts/VRVideoPlayer.vue';
import StdVideoPlayer from './views/layouts/StdVideoPlayer.vue';
import VueApexCharts from 'vue3-apexcharts';
import draggable from 'vuedraggable';

import VueGtag from 'vue-gtag';

// Custom components
import Badge from './components/Badge.vue';
import Notifications from './components/NotificationPlugin';
import SideBar from './components/SidebarPlugin';
import Slider from './components/Slider.vue';
import Modal from './components/Modal.vue';

import 'moment/dist/locale/ja';
import './packs/application.css';
import 'fluid-player/src/css/fluidplayer.css';
import store from './store';

// パスワードの強度チェック
import zxcvbn from 'zxcvbn';

// VeeValidate.setMode('eager');

import routes from './router';

const router = createRouter({
  history: createWebHistory('/'),
  routes,
  linkExactActiveClass: 'nav-item active',
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});

const app = createApp(App);
app.component('badge', Badge);
app.component('vr-video-player', VRVideoPlayer);
app.component('std-video-player', StdVideoPlayer);
app.component('heatmap-video', HeatmapVideo);
app.component('apexchart', VueApexCharts);
app.component('vue-tags-input', VueTagsInput);
app.component('draggable', draggable);
app.use(router);
// app.use(VueMaterial);
app.use(Notifications);
app.use(SideBar);

app.use(InfiniteLoading);
app.config.globalProperties.$moment = moment;
app.use(VueTagsInput);
app.use(QrcodeVue);
app.use(VueApexCharts);

// // Md-materialバグのworkaround
// // https://jollygoodinc.atlassian.net/browse/SQA-132
// // 参考 https://github.com/vuematerial/vue-material/issues/2285
// Vue.component(
//   'MdInput',
//   Vue.options.components.MdInput.extend({
//     methods: {
//       isInvalidValue: function isInvalidValue() {
//         return this.$el.validity
//           ? this.$el.validity.badInput
//           : this.$el.querySelector('input').validity.badInput;
//       },
//     },
//   })
// );

app.config.ignoredElements = [
  'a-scene',
  'a-assets',
  'a-sky',
  'a-camera',
  'a-cursor',
  'a-animation',
  'a-entity',
  'a-box',
  'a-sphere',
  'a-videosphere',
];

import HomeLayout from './views/layouts/HomeLayout.vue';
import App from './App.vue';

app.use(HomeLayout);

app.component('model-list-select', ModelListSelect);
import SearchBox from './views/layouts/SearchBox.vue';
app.component('slider', Slider);
app.component('search-box', SearchBox);
app.component('modal', Modal);

app.use(i18n);

app.use(VueGtag, {
  config: {
    id: 'UA-97322429-18',
  },
  router,
});

app.mixin({
  methods: {
    isProduction() {
      // TODO: 本番環境のときtrueを返す
      return false;
    },
    displayErrorNotify(e) {
      if (e.response) {
        if (e.response.status == 500) {
          this.$notify({
            title: this.$t('internal_server_error'),
            type: 'danger',
          });
        } else if (e.response.status == 422) {
          sessionStorage.setItem(
            'uri',
            '/console/' + this.$router.history.current.fullPath
          );
          sessionStorage.setItem('message', this.$t('csrf_message'));
          sessionStorage.setItem('type', 'danger');
          document.location = '/accounts/sign_out';
        } else {
          var title = e.response.data.message;
          var message = e.response.data.errors.join('<br />');
          if (title == message) {
            // タイトルとメッセージが同じときメッセージは表示しない
            message = null;
          }

          this.$notify({
            title: title,
            message: message,
            type: 'danger',
          });
        }
      } else {
        this.$notify({
          title: e.message,
          type: 'danger',
        });
      }
    },
    copyTextToClipboard(value) {
      var textarea = document.createElement('textarea');
      textarea.textContent = value;

      var body = document.getElementsByTagName('body')[0];
      body.appendChild(textarea);

      textarea.select();
      var result = document.execCommand('copy');
      body.removeChild(textarea);
      return result;
    },
    fileAPISupport() {
      // File APIがサポートされている場合はtrueを返す
      return (
        typeof File !== 'undefined' &&
        typeof Blob !== 'undefined' &&
        typeof FileList !== 'undefined' &&
        (!!Blob.prototype.slice ||
          !!Blob.prototype.webkitSlice ||
          !!Blob.prototype.mozSlice ||
          false)
      );
    },
    passwordStrength(value) {
      // パスワードの強度を返す
      var score = zxcvbn(value).score;
      if (score == 4) {
        return {
          score: 4,
          level: 2,
          message: '強 (Strong）',
          color: '#009900',
        };
      } else if (score == 3) {
        return {
          score: 3,
          level: 1,
          message: '中 (Medium）',
          color: '#fdb514',
        };
      } else {
        return {
          score: score,
          level: 0,
          message: '弱 (Weak）',
          color: '#ff0000',
        };
      }
    },
  },
});

app.use(store);
app.mount('#app');
