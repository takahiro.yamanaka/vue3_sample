import "es6-promise/auto";
import Vue from "vue";
import VueMaterial from "vue-material";
import axios from "axios";
import VueAxiosPlugin from "./vue-axios";
import VueMoment from "vue-moment";
import moment from "moment-timezone";
import VueI18n from "vue-i18n";
import locales from "./i18n";

Vue.use(VueMaterial);
Vue.use(VueAxiosPlugin, { axios: axios });
Vue.use(VueMoment);
Vue.use(VueMoment, { moment });
Vue.use(VueI18n);

// views
import ValidatorsIndex from "../views/validators/ValidatorsIndex.vue";

const i18n = new VueI18n({
  locale: "ja",
  fallbackLocale: "en",
  messages: locales,
});

Vue.filter("moment", function (date) {
  if (date == null || date == "") {
    return "";
  } else if (i18n.locale == "ja") {
    return moment(date).format("YYYY年MM月DD日 HH:mm z");
  } else {
    return moment(date).format("YYYY/MM/DD HH:mm z");
  }
});

Vue.filter("last_updated", function (date) {
  return moment(date).format("MMDD-HHmm");
});

Vue.filter("number_to_human_size", function (size) {
  var units = [" B", " KB", " MB", " GB", " TB"];

  for (var i = 0; size > 1024; i++) {
    size /= 1024;
  }

  return Math.round(size * 100) / 100 + units[i];
});

new Vue({
  i18n,
  render: (h) => h(ValidatorsIndex),
  data: {},
}).$mount("#app");
