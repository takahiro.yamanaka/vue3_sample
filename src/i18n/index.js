import { createI18n } from 'vue-i18n';
import locales from './i18n';

const i18n = new createI18n({
  legacy: false,
  locale: 'ja',
  allowComposition: true,
  fallbackLocale: 'en',
  messages: locales,
});

export default i18n;
