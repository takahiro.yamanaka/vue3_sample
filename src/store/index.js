import { createApp } from 'vue';
import { createStore } from 'vuex';
import App from '../App.vue';
import axios from 'axios';
import createPersistedState from 'vuex-persistedstate';

const state = {
  current_user: null,
  user_state: null,
  console_configs: null,
  prev_path: null,
};

const getters = {
  currentUser: (state) => state.current_user,
  consoleConfigs: (state) => state.console_configs,
  userState: (state) => state.user_state,
  userSignedIn: (state) => !!state.user_state,
  prevPath: (state) => state.prev_path,
};

// Secured Axios
const securedAxios = axios.create({
  // baseURL: 'http://localhost:3000',
  withCredentials: false,
  headers: {
    grant_type: 'password',
    Authorization: `Bearer ${
      localStorage.getItem('user_state') &&
      JSON.parse(localStorage.getItem('user_state')).access_token
    }`,
  },
});
securedAxios.defaults.baseURL = import.meta.env.VITE_API_BASE_URL;
securedAxios.defaults.headers.common['Content-Type'] = 'multipart/form-data';
securedAxios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
securedAxios.defaults.headers.common['Access-Control-Allow-Credentials'] =
  'true';
securedAxios.interceptors.request.use((config) => {
  const method = config.method.toUpperCase();
  if (method !== 'OPTIONS') {
    const token = `${
      localStorage.getItem('user_state') &&
      JSON.parse(localStorage.getItem('user_state')).access_token
    }`;
    config.headers = {
      ...config.headers,
      Authorization: `Bearer ${token}`,
      grant_type: 'password',
    };
  }
  return config;
});

// Response interceptor for API calls
securedAxios.interceptors.response.use(
  (response) => {
    return response;
  },
  async (error) => {
    const originalRequest = error.config;
    if (!error.response) {
      return Promise.reject(
        new Error(
          '通信に失敗しました。インターネットの接続状況を確認し再度お試しください。'
        )
      );
    } else if (error.response.status === 401) {
      const token = `${
        localStorage.getItem('user_state') &&
        JSON.parse(localStorage.getItem('user_state')).refresh_token
      }`;
      if (!token) {
        return Promise.reject(new Error('ログインしてください'));
      }

      return await securedAxios
        .post('/front_api/accounts/token', { refresh_token: token })
        .then((res) => {
          console.log('[info][refresh_token] successfully refreshed');
          const user_state = res.data;
          localStorage.setItem('user_state', JSON.stringify(user_state));
          return securedAxios(originalRequest);
        })
        .catch((err) => {
          console.log('[info][refresh_token] failed');
          return Promise.reject(err);
        });
    } else {
      return Promise.reject(error);
    }
  }
);
///

const actions = {
  // eslint-disable-next-line no-unused-vars
  setUserState({ commit }, payload) {
    commit('SET_USER_STATE', payload);
  },
  // Public Libraries
  // eslint-disable-next-line no-unused-vars
  async fetchLibrariesTop({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/libraries/top`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchLibrary({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/libraries/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchLibraries({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/libraries`, { params })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  // User State
  async fetchCurrentUser({ commit, dispatch }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/users/@me`)
        .then((res) => {
          const current_user = res.data;
          commit('SET_CURRENT_USER', current_user);
          resolve(res);
        })
        .catch((err) => {
          if (err == 'Error: Request failed with status code 401') {
            // session has been expired from serverside
            dispatch('deleteSession');
          }
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async postConsoleUserChangeRole({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/users/change_role`, params)
        .then((res) => {
          const configs = res.data;
          commit('SET_CONSOLE_CONFIGS', configs);
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleConfigs({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/configs`)
        .then((res) => {
          const configs = res.data;
          commit('SET_CONSOLE_CONFIGS', configs);
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchLibrariesConfigs({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/libraries/configs`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchSignupConfigs({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get('/front_api/sign_up/configs')
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // JOLLYGOOD+申し込みフォームのURLを送付する
  // eslint-disable-next-line no-unused-vars
  async sendSignUpFormUrl({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/sign_up/send_form_url`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // JOLLYGOOD+申し込みフォーム送信
  // eslint-disable-next-line no-unused-vars
  async postSignupPlus({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/sign_up/jollygoodplus`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // teams
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleTeams({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/teams`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async deleteConsoleTeam({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .delete(`/front_api/console/teams`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async patchConsoleTeam({ commit }, team) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .patch(`/front_api/console/teams`, { team })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  // Plans
  // eslint-disable-next-line no-unused-vars
  async postConsolePlan({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/plans`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchConsolePlansPayment({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/plans/payment`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  async deleteConsolePlan() {
    return await new Promise((resolve, reject) => {
      securedAxios
        .delete(`/front_api/console/plans/destroy`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async postConsolePlanCancel({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/plans/cancel`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async postConsolePlanSuspension({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/plans/suspension`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchConsolePlanInvoices({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/plans/invoices`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  // users
  // eslint-disable-next-line no-unused-vars
  async fetchTwoFactorAuthentication({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/users/two_factor_authentication`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async postTwoFactorAuthentication({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(
          `/front_api/console/users/enable_two_factor_authentication`,
          params
        )
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async deleteTwoFactorAuthentication({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .delete(`/front_api/console/users/disable_two_factor_authentication`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleUser({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/users/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleUsers({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/users`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async upsertConsoleUser({ commit }, params) {
    var id = params.get('id');
    if (id == '') {
      return await new Promise((resolve, reject) => {
        securedAxios
          .post(`/front_api/console/users`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    } else {
      return await new Promise((resolve, reject) => {
        securedAxios
          .patch(`/front_api/console/users/${id}`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    }
  },
  // eslint-disable-next-line no-unused-vars
  async deleteConsoleUser({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .delete(`/front_api/console/users/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  // vr recs
  async fetchConsoleVrRecs({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/vr_recs`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  async fetchConsoleVrRec({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/vr_recs/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  async patchConsoleVrRec({ commit }, params) {
    var id = params.get('id');
    return await new Promise((resolve, reject) => {
      securedAxios
        .patch(`/front_api/console/vr_recs/${id}`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  async deleteConsoleVrRec({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .delete(`/front_api/console/vr_recs/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  // libraries type
  // eslint-disable-next-line no-unused-vars
  async fetchLibrariesAllType({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/libraries/all_type`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // Login ログイン
  // eslint-disable-next-line no-unused-vars
  async postSession({ commit }, params) {
    const client_auth = {
      grant_type: 'password',
      client_id: `${import.meta.env.VITE_API_CLIENT_ID}`,
      client_secret: `${import.meta.env.VITE_API_CLIENT_SECRET}`,
    };
    params = { ...params, ...client_auth };
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/accounts/sign_in`, params)
        .then((res) => {
          const user_state = res.data;
          commit('SET_USER_STATE', user_state);
          localStorage.setItem('user_state', JSON.stringify(user_state));
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // Login ログイン
  // eslint-disable-next-line no-unused-vars
  async postProxyLogin({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post('/front_api/accounts/proxy_login', params)
        .then((res) => {
          const user_state = res.data;
          commit('SET_USER_STATE', user_state);
          localStorage.setItem('user_state', JSON.stringify(user_state));
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // Logout ログアウト
  // eslint-disable-next-line no-unused-vars
  async deleteSession({ commit }) {
    console.log('[debug] ログアウト Begins');
    commit('SET_USER_STATE', null);
    commit('SET_CURRENT_USER', null);
    return await new Promise((resolve, reject) => {
      // TODO: FIXME: 要尾動作確認、コントローラーpathがtokensではないか？
      securedAxios
        .delete('/front_api/accounts/sign_out')
        .then((res) => {
          console.log('[debug] ログアウト Success');
          resolve(res);
        })
        .catch((err) => {
          console.log('[debug] ログアウト Fail');
          reject(err);
        })
        .finally(() => {
          // セッション情報を削除する
          commit('SET_USER_STATE', null);
          localStorage.removeItem('user_state');
        });
    });
  },
  // パスワード再設定メール
  // eslint-disable-next-line no-unused-vars
  async postForgotPasswordMail({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post('/front_api/accounts/send_forgot_password_mail', params)
        .then((e) => {
          resolve(e);
        })
        .catch((e) => {
          reject(e);
        });
    });
  },
  // パスワード変更
  // eslint-disable-next-line no-unused-vars
  async postChangePassword({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post('/front_api/accounts/change_password', params)
        .then((e) => {
          resolve(e);
        })
        .catch((e) => {
          reject(e);
        });
    });
  },
  // パスワード変更
  // eslint-disable-next-line no-unused-vars
  async postChangeEmail({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post('/front_api/accounts/change_email', params)
        .then((e) => {
          resolve(e);
        })
        .catch((e) => {
          reject(e);
        });
    });
  },
  // 利用規約取得
  // eslint-disable-next-line no-unused-vars
  async fetchTerms({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get('/front_api/accounts/terms', { params })
        .then((e) => {
          resolve(e);
        })
        .catch((e) => {
          reject(e);
        });
    });
  },
  // 利用規約同意
  // eslint-disable-next-line no-unused-vars
  async postTerms({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post('/front_api/accounts/terms', params)
        .then((e) => {
          resolve(e);
        })
        .catch((e) => {
          reject(e);
        });
    });
  },
  // libraries_scenarios
  // eslint-disable-next-line no-unused-vars
  async fetchLibrariesScenario({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/libraries/scenario/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  // Write a review
  // eslint-disable-next-line no-unused-vars
  async postScenarioReview({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/reviews`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async deleteScenarioReview({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .delete(`/front_api/console/reviews/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchLibrariesReview({ commit }, params) {
    const id = params.id;
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/libraries/reviews/${id}`, { params })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  async fetchLibrariesReviewPublic({ commit }, params) {
    const id = params.id;
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/libraries/reviews_public/${id}`, { params })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  async fetchConsoleReviews({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/reviews/`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  // eslint-disable-next-line no-unused-vars
  async fetchConsoleNews({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/news`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async postConsoleReviewReaction({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/review_reactions/`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // TODO: 使用されていない！？
  // eslint-disable-next-line no-unused-vars
  // async postReviews({ commit }) {
  //   return await new Promise((resolve, reject) => {
  //     securedAxios
  //       .post(`/front_api/console/reviews/`)
  //       .then((res) => {
  //         resolve(res);
  //       })
  //       .catch((err) => {
  //         reject(err);
  //       });
  //   });
  // },

  // contents
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleContentsSetting({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/contents_setting`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async postConsoleContentsSetting({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/contents_setting`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  // devices
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleDevice({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/devices/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleDevices({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/devices`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async deleteConsoleDevice({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .delete(`/front_api/console/devices/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async upsertConsoleDevice({ commit }, params) {
    var method = 'post';
    if (params.id != '') {
      method = 'patch';
    }
    if (method == 'post') {
      return await new Promise((resolve, reject) => {
        securedAxios
          .post(`/front_api/console/devices`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    } else {
      return await new Promise((resolve, reject) => {
        securedAxios
          .patch(`/front_api/console/devices/${params.id}`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    }
  },
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleDeviceInvoices({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/device_purchases/invoices`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleTeamDevices({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/team_devices`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async postConsoleDevicePurchase({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/device_purchases/`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async postConsoleDevicesBulk({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/devices/bulk/`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  // TeamDevices
  // eslint-disable-next-line no-unused-vars
  async postConsoleTeamDevicesBulk({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/team_devices/bulk`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async patchConsoleTeamDevicesUser({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .patch(`/front_api/console/team_devices/change_user`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleTeamDevice({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/team_devices/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async deleteConsoleTeamDevice({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .delete(`/front_api/console/team_devices/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async deleteConsoleTeamDevicesBulk({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .delete(`/front_api/console/team_devices/destroy_bulk`, {
          data: params,
        })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async upsertConsoleTeamDevice({ commit }, params) {
    var method = 'post';
    if (params.id != '') {
      method = 'patch';
    }
    if (method == 'post') {
      return await new Promise((resolve, reject) => {
        securedAxios
          .post(`/front_api/console/team_devices`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    } else {
      return await new Promise((resolve, reject) => {
        securedAxios
          .patch(`/front_api/console/team_devices/${params.id}`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    }
  },

  // students
  // eslint-disable-next-line no-unused-vars
  async postConsoleStudent({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/students`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async postConsoleStudentBulk({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/students/bulk`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  // classrooms
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleClassroom({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/classrooms/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchClassrooms({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/classrooms`, { params })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async upsertClassrooms({ commit }, params) {
    var id = params.get('id');
    return await new Promise((resolve, reject) => {
      securedAxios
        .patch(`/front_api/console/classrooms/${id}`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchClassroomStudents({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/classrooms/by_students`, { params })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async postClassroomByStudents({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/classrooms/by_students`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  // Students
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleStudent({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/students/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async deleteConsoleStudent({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .delete(`/front_api/console/students/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleStudents({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/students`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async patchConsoleStudent({ commit }, params) {
    var id = params.get('id');
    return await new Promise((resolve, reject) => {
      securedAxios
        .patch(`/front_api/console/students/${id}`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async postClassroomsByNumberOfPeople({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/classrooms/by_number_of_people/`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleEvents({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/events`, { params })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  // Channels
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleChannel({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/channels/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleChannels({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/channels`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async upsertConsoleChannel({ commit }, params) {
    var id = params.get('id');
    if (id == '') {
      return await new Promise((resolve, reject) => {
        securedAxios
          .post(`/front_api/console/channels`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    } else {
      return await new Promise((resolve, reject) => {
        securedAxios
          .patch(`/front_api/console/channels/${id}`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    }
  },
  // チャンネル配下の要素を削除する
  // eslint-disable-next-line no-unused-vars
  async deleteConsoleChannelTarget({ commit }, params) {
    // paramsはパス
    return await new Promise((resolve, reject) => {
      securedAxios
        .delete(`/front_api/console/${params}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // Videos
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleVideoTracking({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/videos/${params.id}/tracking`, { params })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleVideoHeatmap({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/videos/${id}/heatmap`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleVideo({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/videos/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async patchConsoleVideo({ commit }, params) {
    var id = params.get('id');
    return await new Promise((resolve, reject) => {
      securedAxios
        .patch(`/front_api/console/videos/${id}`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async deleteConsoleVideo({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .delete(`/front_api/console/videos/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleVideos({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/videos`, { params })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async postConsoleVideoUpload({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/videos/put_upload`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async postConsoleVideoCreate({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/videos/create`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async postConsoleVideoStartMultipartUpload({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/videos/start_multipart_upload`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async postConsoleVideoSigningMultipartUpload({ commit }, params) {
    console.log(
      '[debug] postConsoleVideoSigningMultipartUpload params = ',
      params
    );
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/videos/signing_multipart_upload`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async postConsoleVideoCompleteMultipartUpload({ commit }, params) {
    console.log('[debug] CompleteMultipartUpload params = ', params);
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/videos/complete_multipart_upload`, params)
        .then((res) => {
          console.log('[debug] CompleteMultipartUpload res = ', res);
          resolve(res);
        })
        .catch((err) => {
          console.log('[debug] CompleteMultipartUpload err = ', err);
          reject(err);
        });
    });
  },
  async fetchConsoleVideosList() {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/videos/list`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  async fetchConsoleServers() {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/servers`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async upsertConsoleCamera({ commit }, params) {
    var method = 'post';
    if (params.id != '') {
      method = 'patch';
    }
    if (method == 'post') {
      console.log('[debug] upsertConsoleCamera post', params);
      return await new Promise((resolve, reject) => {
        securedAxios
          .post(`/front_api/console/cameras`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    } else if (method == 'patch') {
      console.log('[debug] upsertConsoleCamera patch', params);
      return await new Promise((resolve, reject) => {
        securedAxios
          .patch(`/front_api/console/cameras/${params.id}`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    }
  },
  // eslint-disable-next-line no-unused-vars
  async patchConsoleVideoUpload({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .patch(`/front_api/console/video_upload_requests/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  // Categories
  // eslint-disable-next-line no-unused-vars
  async upsertConsoleCategory({ commit }, params) {
    var id = params.get('id');
    if (id == '') {
      return await new Promise((resolve, reject) => {
        securedAxios
          .post(`/front_api/console/categories`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    } else {
      return await new Promise((resolve, reject) => {
        securedAxios
          .patch(`/front_api/console/categories/${id}`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    }
  },
  // Scenarios
  // eslint-disable-next-line no-unused-vars
  async upsertConsoleScenario({ commit }, params) {
    var id = params.get('id');
    if (id == '') {
      return await new Promise((resolve, reject) => {
        securedAxios
          .post(`/front_api/console/scenarios`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    } else {
      return await new Promise((resolve, reject) => {
        securedAxios
          .patch(`/front_api/console/scenarios/${id}`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    }
  },

  // Slots
  // eslint-disable-next-line no-unused-vars
  async postConsoleSlotsCopy({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/slots/copy`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async upsertConsoleSlot({ commit }, params) {
    var id = params.get('id');
    if (id == '') {
      return await new Promise((resolve, reject) => {
        securedAxios
          .post(`/front_api/console/slots`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    } else {
      return await new Promise((resolve, reject) => {
        securedAxios
          .patch(`/front_api/console/slots/${id}`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    }
  },
  // Buttons
  // eslint-disable-next-line no-unused-vars
  async upsertConsoleButton({ commit }, params) {
    var id = params.get('id');
    if (id == '') {
      return await new Promise((resolve, reject) => {
        securedAxios
          .post(`/front_api/console/buttons`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    } else {
      return await new Promise((resolve, reject) => {
        securedAxios
          .patch(`/front_api/console/buttons/${id}`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    }
  },
  // eslint-disable-next-line no-unused-vars
  async postConsoleScenariosCopy({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .post(`/front_api/console/scenarios/copy`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleScenarioTags({ commit }) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/scenarios/tags`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  // Quiz
  // eslint-disable-next-line no-unused-vars
  async upsertConsoleQuiz({ commit }, params) {
    var method = 'post';
    if (params.id != '') {
      method = 'patch';
    }
    if (method == 'post') {
      return await new Promise((resolve, reject) => {
        securedAxios
          .post(`/front_api/console/quizzes`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    } else {
      return await new Promise((resolve, reject) => {
        securedAxios
          .patch(`/front_api/console/quizzes/${params.id}`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    }
  },

  // Markers
  // eslint-disable-next-line no-unused-vars
  async upsertConsoleMarker({ commit }, params) {
    var method = 'post';
    if (params.id != '') {
      method = 'patch';
    }
    if (method == 'post') {
      return await new Promise((resolve, reject) => {
        securedAxios
          .post(`/front_api//console/markers`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    } else {
      return await new Promise((resolve, reject) => {
        securedAxios
          .patch(`/front_api//console/markers/${params.id}`, params)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    }
  },

  // Scores
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleScores({ commit }, id) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/scores/${id}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async fetchConsoleScoresByUserId({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/console/scores/${params.classroomId}/${params.userId}`)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async patchConsoleClassroomComments({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .patch(`/front_api/console/classrooms/comments`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // eslint-disable-next-line no-unused-vars
  async patchConsoleClassroomInternalComments({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .patch(`/front_api/console/classrooms/internal_comments`, params)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // EmbeddedVideo
  // eslint-disable-next-line no-unused-vars
  async fetchEmbeddedVideo({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/embedded_video/`, { params })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // SharedVideo
  // eslint-disable-next-line no-unused-vars
  async fetchSharedVideo({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/shared_video/`, { params })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // ChannelValidator
  // eslint-disable-next-line no-unused-vars
  async fetchChannelValidator({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/channel_validator/`, { params })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // Zendesk SSO
  // eslint-disable-next-line no-unused-vars
  async fetchZendeskSso({ commit }, params) {
    return await new Promise((resolve, reject) => {
      securedAxios
        .get(`/front_api/accounts/zendesk_sso/`, { params })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // Login/out
  // eslint-disable-next-line no-unused-vars
  storeCurrentPath({ commit }, path) {
    // GURUVR-6532: レビュー投稿ページはシナリオページへredirectさせる
    commit('SET_PREV_PATH', path);
  },
};

const mutations = {
  SET_USER_STATE(state, user) {
    state.user_state = user;
  },
  SET_CURRENT_USER(state, current_user) {
    state.current_user = current_user;
  },
  SET_CONSOLE_CONFIGS(state, configs) {
    state.console_configs = configs;
  },
  SET_PREV_PATH(state, path) {
    state.prev_path = path;
  },
};

const dataStore = {
  state,
  getters,
  actions,
  mutations,
};

const dataState = createPersistedState(dataStore);

export default createStore({
  modules: {
    dataStore,
  },
  plugins: [dataState],
});
