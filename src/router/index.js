import HomeLayout from '../views/layouts/HomeLayout.vue';
import DashboardLayout from '../views/layouts/DashboardLayout.vue';

// home
import LibrariesIndex from '../views/page/LibrariesIndex.vue';
import LibrariesScenarioShow from '../views/page/LibrariesScenarioShow.vue';

// // accounts
import SignIn from '../views/accounts/SignIn.vue';
import SignOut from '../views/accounts/SignOut.vue';
// import ForgotPassword from '../views/accounts/ForgotPassword.vue';
// import ChangePassword from '../views/accounts/ChangePassword.vue';
// import ChangeEmail from '../views/accounts/ChangeEmail.vue';
import TermsShow from '../views/accounts/TermsShow.vue';
// import SignUpJollygoodplus from '../views/accounts/SignUpJollygoodplus.vue';
// import SignUp from '../views/accounts/SignUp.vue';
// import ZendeskSso from '../views/accounts/ZendeskSso.vue';
// import ProxyLogin from '../views/accounts/ProxyLogin.vue';

// // libraries
import ConsoleLibrariesIndex from '../views/console/LibrariesIndex.vue';
// import ConsoleLibrariesScenarioShow from '../views/console/LibrariesScenarioShow.vue';
// import ConsoleLibrariesScenarioReviewsNew from '../views/console/LibrariesScenarioReviewsNew.vue';
// import ConsoleLibrariesShow from '../views/console/LibrariesShow.vue';
// import ActivityIndex from '../views/console/ActivityIndex.vue';

// // Purchases
// import DevicePurchasesNew from '../views/console/DevicePurchasesNew.vue';
// import DevicePurchasesInvoices from '../views/console/DevicePurchasesInvoices.vue';

// // Scores
// import ScoresUser from '../views/console/ScoresUser.vue';
// import ScoresClass from '../views/console/ScoresClass.vue';
// // contents
// import ContentsSetting from '../views/console/ContentsSetting.vue';

// // devices
// import DevicesIndex from '../views/console/DevicesIndex.vue';
// import DevicesEdit from '../views/console/DevicesEdit.vue';
// import DevicesBulk from '../views/console/DevicesBulk.vue';
// import TeamDevicesIndex from '../views/console/TeamDevicesIndex.vue';
// import TeamDevicesEdit from '../views/console/TeamDevicesEdit.vue';
// import TeamDevicesBulk from '../views/console/TeamDevicesBulk.vue';

// // teams
// import TeamsIndex from '../views/console/TeamsIndex.vue';
// import TeamsEdit from '../views/console/TeamsEdit.vue';

// // users
// import UsersIndex from '../views/console/UsersIndex.vue';
// import UsersShow from '../views/console/UsersShow.vue';
// import UsersEdit from '../views/console/UsersEdit.vue';
// import UsersTwoFactor from '../views/console/UsersTwoFactor.vue';
// // Plans
// import PlansNew from '../views/console/PlansNew.vue';
// import PlansSuspension from '../views/console/PlansSuspension.vue';
// import PlansCancel from '../views/console/PlansCancel.vue';
// import PlansInvoices from '../views/console/PlansInvoices.vue';
// import PlansPayment from '../views/console/PlansPayment.vue';

// // VrRec
// import VrRecsIndex from '../views/console/VrRecsIndex.vue';
// import VrRecsEdit from '../views/console/VrRecsEdit.vue';

// MediaPro component
// const VideosIndex = () => import('../views/console/VideosIndex.vue');
// const VideosNew = () => import('../views/console/VideosNew.vue');
// const VideosNewByMultipartUpload = () =>
//   import('../views/console/VideosNewByMultipartUpload.vue');
// const VideosEdit = () => import('../views/console/VideosEdit.vue');
// const VideosShow = () => import('../views/console/VideosShow.vue');
// // Class component
// const ClassroomsShow = () => import('../views/console/ClassroomsShow.vue');
// const ClassroomsIndex = () => import('../views/console/ClassroomsIndex.vue');
// const ClassroomsEdit = () => import('../views/console/ClassroomsEdit.vue');
// const ClassroomsByNumberOfPeople = () =>
//   import('../views/console/ClassroomsByNumberOfPeople.vue');
// const ClassroomsByStudents = () =>
//   import('../views/console/ClassroomsByStudents.vue');
// // Home component
// const MyClassrooms = () => import('../views/console/MyClassrooms.vue');

// // Buttons
// const ButtonsEdit = () => import('../views/console/ButtonsEdit.vue');

// // Students
// const StudentsIndex = () => import('../views/console/StudentsIndex.vue');
// const StudentsEdit = () => import('../views/console/StudentsEdit.vue');
// const StudentsShow = () => import('../views/console/StudentsShow.vue');
// const StudentsBulk = () => import('../views/console/StudentsBulk.vue');

// // Channels
// const ChannelsIndex = () => import('../views/console/ChannelsIndex.vue');
// const ChannelsEdit = () => import('../views/console/ChannelsEdit.vue');
// const ChannelsShow = () => import('../views/console/ChannelsShow.vue');
// const CategoriesEdit = () => import('../views/console/CategoriesEdit.vue');
// const ScenariosEdit = () => import('../views/console/ScenariosEdit.vue');

// // Items(slots)
// const SlotsEdit = () => import('../views/console/SlotsEdit.vue');
// const SlotsShow = () => import('../views/console/SlotsShow.vue');

// // Quizzes
// const QuizzesEdit = () => import('../views/console/QuizzesEdit.vue');
// const QuizzesShow = () => import('../views/console/QuizzesShow.vue');
// const MarkersEdit = () => import('../views/console/MarkersEdit.vue');

// const ServersIndex = () => import('../views/console/ServersIndex.vue');
// const ServersShow = () => import('../views/console/ServersShow.vue');

// const CamerasEdit = () => import('../views/console/CamerasEdit.vue');

// EmbeddedVideo and SharedVideo
const EmbeddedVideo = () => import('../views/page/EmbeddedVideo.vue');
const SharedVideo = () => import('../views/page/SharedVideo.vue');

const ChannelValidator = () => import('../views/page/ChannelValidator.vue');

const Status40x = () => import('../views/page/Status40x.vue');

const routes = [
  {
    path: '/',
    component: HomeLayout,
    children: [
      {
        path: '',
        name: 'public_libraries',
        component: LibrariesIndex,
      },
      {
        path: 'libraries/scenario/:id',
        name: 'public_show_libraries_scenario',
        components: { default: LibrariesScenarioShow },
      },
      {
        path: 'sign_in',
        redirect: '/accounts/sign_in',
      },
      {
        path: 'forgot_password',
        redirect: '/accounts/forgot_password',
      },
      {
        path: '.well-known/change-password',
        redirect: '/accounts/forgot_password',
      },
    ],
  },
  {
    path: '/embeds/',
    name: 'embedded_video',
    component: EmbeddedVideo,
  },
  {
    path: '/videos/:id',
    name: 'shared_video',
    component: SharedVideo,
  },
  {
    path: '/validators/',
    name: 'channel_validators',
    component: ChannelValidator,
  },
  {
    path: '/console/',
    component: DashboardLayout,
    children: [
      //     {
      //       path: 'teams',
      //       name: 'contractor_info',
      //       components: { default: TeamsIndex },
      //     },
      //     {
      //       path: 'teams/edit',
      //       name: 'edit_contractor_info',
      //       components: { default: TeamsEdit },
      //     },
      //     {
      //       path: 'videos',
      //       name: 'videos',
      //       components: { default: VideosIndex },
      //     },
      //     {
      //       path: 'videos/new',
      //       name: 'new_video',
      //       components: { default: VideosNew },
      //     },
      //     {
      //       path: 'videos/new_by_multipart_upload',
      //       name: 'new_video_by_multipart_upload',
      //       components: { default: VideosNewByMultipartUpload },
      //     },
      //     {
      //       path: 'videos/:id',
      //       name: 'show_video',
      //       components: { default: VideosShow },
      //     },
      //     {
      //       path: 'videos/:id/edit',
      //       name: 'edit_video',
      //       components: { default: VideosEdit },
      //     },
      //     {
      //       path: 'users/',
      //       name: 'users',
      //       components: { default: UsersIndex },
      //     },
      //     {
      //       path: 'users/new',
      //       name: 'new_user',
      //       components: { default: UsersEdit },
      //     },
      //     {
      //       path: 'users/two_factor',
      //       name: 'two_factor_authentication',
      //       components: { default: UsersTwoFactor },
      //     },
      //     {
      //       path: 'users/:id',
      //       name: 'show_user',
      //       components: { default: UsersShow },
      //     },
      //     {
      //       path: 'users/:id/edit',
      //       name: 'edit_user',
      //       components: { default: UsersEdit },
      //     },
      //     {
      //       path: 'channels',
      //       name: 'channels',
      //       components: { default: ChannelsIndex },
      //     },
      //     {
      //       path: 'channels/new',
      //       name: 'new_channel',
      //       components: { default: ChannelsEdit },
      //     },
      //     {
      //       path: 'channels/:id/edit',
      //       name: 'edit_channel',
      //       components: { default: ChannelsEdit },
      //     },
      //     {
      //       path: 'channels/:id',
      //       name: 'show_channel',
      //       components: { default: ChannelsShow },
      //     },
      //     {
      //       path: 'channels/:channel_id/categories/new',
      //       name: 'new_category',
      //       components: { default: CategoriesEdit },
      //     },
      //     {
      //       path: 'channels/:channel_id/categories/:id/edit',
      //       name: 'edit_category',
      //       components: { default: CategoriesEdit },
      //     },
      //     {
      //       path: 'channels/:channel_id/categories/:category_id/scenarios/new',
      //       name: 'new_scenario',
      //       components: { default: ScenariosEdit },
      //     },
      //     {
      //       path: 'channels/:channel_id/scenarios/:id/edit',
      //       name: 'edit_scenario',
      //       components: { default: ScenariosEdit },
      //     },
      //     {
      //       path: 'channels/:channel_id/scenarios/:scenario_id/slots/new',
      //       name: 'new_slot',
      //       components: { default: SlotsEdit },
      //     },
      //     {
      //       path: 'channels/:channel_id/slots/:id/edit',
      //       name: 'edit_slot',
      //       components: { default: SlotsEdit },
      //     },
      //     {
      //       path: 'channels/:channel_id/slots/:id',
      //       name: 'show_slot',
      //       components: { default: SlotsShow },
      //     },
      //     {
      //       path: 'channels/:channel_id/slots/:slot_id/quizzes/new',
      //       name: 'new_quiz',
      //       components: { default: QuizzesEdit },
      //     },
      //     {
      //       path: 'channels/:channel_id/quizzes/:id/edit',
      //       name: 'edit_quiz',
      //       components: { default: QuizzesEdit },
      //     },
      //     {
      //       path: 'channels/:channel_id/quizzes/:id',
      //       name: 'show_quiz',
      //       components: { default: QuizzesShow },
      //     },
      //     {
      //       path: 'channels/:channel_id/quizzes/:quiz_id/markers/new',
      //       name: 'new_marker',
      //       components: { default: MarkersEdit },
      //     },
      //     {
      //       path: 'channels/:channel_id/markers/:id/edit',
      //       name: 'edit_marker',
      //       components: { default: MarkersEdit },
      //     },
      //     {
      //       path: 'channels/:channel_id/slots/:slot_id/buttons/new',
      //       name: 'new_button',
      //       components: { default: ButtonsEdit },
      //     },
      //     {
      //       path: 'channels/:channel_id/buttons/:id/edit',
      //       name: 'edit_button',
      //       components: { default: ButtonsEdit },
      //     },
      //     {
      //       path: 'classrooms/',
      //       name: 'classrooms',
      //       components: { default: ClassroomsIndex },
      //     },
      //     {
      //       path: 'classrooms/new',
      //       name: 'new_classroom',
      //       components: { default: ClassroomsEdit },
      //     },
      //     {
      //       path: 'classrooms/by_number_of_people',
      //       name: 'classroom_by_number_of_people',
      //       components: { default: ClassroomsByNumberOfPeople },
      //     },
      //     {
      //       path: 'classrooms/by_students',
      //       name: 'classroom_by_students',
      //       components: { default: ClassroomsByStudents },
      //     },
      //     {
      //       path: 'classrooms/:id',
      //       name: 'show_classroom',
      //       components: { default: ClassroomsShow },
      //     },
      //     {
      //       path: 'classrooms/:id/edit',
      //       name: 'edit_classroom',
      //       components: { default: ClassroomsEdit },
      //     },
      //     {
      //       path: 'my_classrooms/',
      //       name: 'my_classrooms',
      //       components: { default: MyClassrooms },
      //     },
      //     {
      //       path: 'devices/',
      //       name: 'devices',
      //       components: { default: DevicesIndex },
      //     },
      //     {
      //       path: 'devices/new',
      //       name: 'new_device',
      //       components: { default: DevicesEdit },
      //     },
      //     {
      //       path: 'devices/:id/edit',
      //       name: 'edit_device',
      //       components: { default: DevicesEdit },
      //     },
      //     {
      //       path: 'devices/bulk',
      //       name: 'bulk_device',
      //       components: { default: DevicesBulk },
      //     },
      //     {
      //       path: 'scores/:classroom_id',
      //       name: 'total_score',
      //       components: { default: ScoresClass },
      //     },
      //     {
      //       path: 'scores/:classroom_id/:user_id',
      //       name: 'individual_score',
      //       components: { default: ScoresUser },
      //     },
      //     {
      //       path: 'contents_setting',
      //       name: 'contents_setting',
      //       components: { default: ContentsSetting },
      //     },
      //     {
      //       path: 'students/',
      //       name: 'students',
      //       components: { default: StudentsIndex },
      //     },
      //     {
      //       path: 'students/bulk',
      //       name: 'bulk_student',
      //       components: { default: StudentsBulk },
      //     },
      //     {
      //       path: 'students/new',
      //       name: 'new_student',
      //       components: { default: StudentsEdit },
      //     },
      //     {
      //       path: 'students/:id/edit',
      //       name: 'edit_student',
      //       components: { default: StudentsEdit },
      //     },
      //     {
      //       path: 'students/:id',
      //       name: 'show_student',
      //       components: { default: StudentsShow },
      //     },
      //     {
      //       path: 'servers',
      //       name: 'cloud_archive',
      //       components: { default: ServersIndex },
      //     },
      //     {
      //       path: 'servers/:id',
      //       name: 'show_server',
      //       components: { default: ServersShow },
      //     },
      //     {
      //       path: 'servers/:server_id/cameras/new',
      //       name: 'new_camera',
      //       components: { default: CamerasEdit },
      //     },
      //     {
      //       path: 'servers/:server_id/cameras/:id/edit',
      //       name: 'edit_camera',
      //       components: { default: CamerasEdit },
      //     },
      //     {
      //       path: 'team_devices/',
      //       name: 'team_devices',
      //       components: { default: TeamDevicesIndex },
      //     },
      //     {
      //       path: 'team_devices/new',
      //       name: 'new_team_device',
      //       components: { default: TeamDevicesEdit },
      //     },
      //     {
      //       path: 'team_devices/:id/edit',
      //       name: 'edit_team_device',
      //       components: { default: TeamDevicesEdit },
      //     },
      //     {
      //       path: 'team_devices/bulk',
      //       name: 'bulk_team_device',
      //       components: { default: TeamDevicesBulk },
      //     },
      {
        path: 'libraries',
        name: 'libraries',
        components: { default: ConsoleLibrariesIndex },
      },
      //     {
      //       path: 'libraries/scenario/:id',
      //       name: 'show_libraries_scenario',
      //       components: { default: ConsoleLibrariesScenarioShow },
      //     },
      //     {
      //       path: 'libraries/scenario/:id/reviews/new',
      //       name: 'new_review',
      //       components: { default: ConsoleLibrariesScenarioReviewsNew },
      //     },
      //     {
      //       path: 'libraries/:id',
      //       name: 'show_library',
      //       components: { default: ConsoleLibrariesShow },
      //     },
      //     {
      //       path: 'activity',
      //       name: 'activity',
      //       components: { default: ActivityIndex },
      //     },
      //     {
      //       path: 'device_purchases/new',
      //       name: 'new_device_purchases',
      //       components: { default: DevicePurchasesNew },
      //     },
      //     {
      //       path: 'device_purchases/invoices',
      //       name: 'device_purchases_invoices',
      //       components: { default: DevicePurchasesInvoices },
      //     },
      //     {
      //       path: 'plans/new',
      //       name: 'new_plan',
      //       components: { default: PlansNew },
      //     },
      //     {
      //       path: 'plans/suspension',
      //       name: 'plan_suspension',
      //       components: { default: PlansSuspension },
      //     },
      //     {
      //       path: 'plans/cancel',
      //       name: 'plan_cancel',
      //       components: { default: PlansCancel },
      //     },
      //     {
      //       path: 'plans/invoices',
      //       name: 'plan_invoices',
      //       components: { default: PlansInvoices },
      //     },
      //     {
      //       path: 'plans/payment',
      //       name: 'plan_payment',
      //       components: { default: PlansPayment },
      //     },
      //     {
      //       path: 'vr_recs/',
      //       name: 'vr_recs',
      //       components: { default: VrRecsIndex },
      //     },
      //     {
      //       path: 'vr_recs/:id/edit',
      //       name: 'edit_vr_recs',
      //       components: { default: VrRecsEdit },
      //     },
      //     {
      //       path: '/:catchAll(.*)',
      //       name: 'dashboard',
      //       redirect: 'libraries',
      //     },
    ],
  },
  {
    path: '/accounts',
    component: HomeLayout,
    children: [
      //     {
      //       path: 'sign_up/jollygoodplus',
      //       name: 'sign_up_jollygoodplus',
      //       component: SignUpJollygoodplus,
      //     },
      //     {
      //       path: 'sign_up',
      //       name: 'sign_up',
      //       component: SignUp,
      //     },
      {
        path: 'sign_in',
        name: 'sign_in',
        component: SignIn,
      },
      //     {
      //       path: 'forgot_password',
      //       name: 'forgot_password',
      //       component: ForgotPassword,
      //     },
      //     {
      //       path: 'change_password',
      //       name: 'change_password',
      //       component: ChangePassword,
      //     },
      //     {
      //       path: 'change_email',
      //       name: 'change_email',
      //       component: ChangeEmail,
      //     },
      {
        path: 'terms',
        name: 'terms_of_service',
        component: TermsShow,
      },
      {
        path: 'sign_out',
        name: 'sign_out',
        component: SignOut,
      },
      //     {
      //       path: 'zendesk_sso',
      //       name: 'zendesk_sso',
      //       component: ZendeskSso,
      //     },
      //     {
      //       path: 'proxy_login',
      //       name: 'proxy_login',
      //       component: ProxyLogin,
      //     },
    ],
  },
  {
    path: '/:catchAll(.*)',
    name: 'status_404',
    component: Status40x,
  },
];
export default routes;
