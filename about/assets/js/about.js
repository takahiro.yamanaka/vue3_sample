$(function () {
  // アバウト トップ画面 スライド
  let swiperOptions = {};

  if ($("#js-about-slide .swiper-slide").length > 1) { //スライドが1枚以上だったら
    swiperOptions = {
      loop: true,
      speed: 2000,
      autoplay: {
        delay: 5000,
      },
    };

  } else { // スライドが1枚の場合はループ、オートプレイ無効
    swiperOptions = {
      loop: false, //ループしない
      autoplay: false, //オートプレイしない
    }
  }
  const swiper = new Swiper("#js-about-slide", swiperOptions);

  // ナビゲーションの内容をスマホ用に複製
  $('#js-header-nav').each(function () {
    var $clone = $(this).children().clone(),
      $navSp = $('#js-header-nav-sp');

    $clone.appendTo($navSp);
  });

  // スマホサブメニュー開閉ボタンクリック
  $('#js-sub-header-ttl').on('click', function (event) {
    var $navSp = $('#js-sub-header-list');

    $(this).toggleClass('open');
    $navSp.slideToggle();
  });

  // スマホメニュー開閉ボタンクリック
  $('.js-sp-menu').on('click', function () {
    var $navSp = $('#js-header-nav-sp'),
      $touchSensor = $('#js-header-nav-sensor'); // ナビゲーション外のタップを感知

    $navSp.toggleClass('open');
    $touchSensor.toggleClass('open');
  });

  // 回答ボックスを表示
  $('.about-question__q').on('click', function () {
    $(this).next().slideToggle();
    $(this).toggleClass('open');
  });

  // トップへ戻るボタン
  $('#js-back2top').each(function () {
    var $this = $(this),
      $window = $(window),
      threshold = $window.height();

    $this.hide();

    // スクロールに合わせて表示/非表示切り替え
    $window.on('scroll', $.throttle(1000 / 15, function () {
      if ($window.scrollTop() > threshold) {
        $this.fadeIn();
      } else {
        $this.fadeOut();
      }
    }));
  });

  // アンカータグクリック時
  var headerHeight = $('header').innerHeight(),
    speed = 1000;

  $('a[href^="#"]').on('click', function () {
    var href = $(this).attr('href'),
      target = $(href === "#" || href === "" ? 'html' : href),
      position = target.offset().top - headerHeight;

    $('body,html').animate({
      scrollTop: position
    }, speed, 'swing');

    return false;

  });

  // スクロールに合わせコンテンツをフェードイン。top.jsより引用。
  $(window).scroll(function () {
    $(".scrollBlock").each(function () {
      var scroll = $(window).scrollTop();
      var blockPosition = $(this).offset().top;
      var windowHeihgt = $(window).height();
      if (scroll > blockPosition - windowHeihgt + 100) {
        $(this).addClass("blockIn");
      }
    });
  });
});

var resizeTimer = false;

jQuery(window).on('resize', function () {

  if (resizeTimer !== false) {
    // リサイズの動作が終わってから処理開始
    clearTimeout(resizeTimer);
  }

  resizeTimer = setTimeout(function () {

    if (jQuery(window).width() >= 768) {
      // メニュー初期化
      $('#js-header-nav-sp').removeClass('open');
      $('#js-header-nav-sensor').removeClass('open');

      // サブメニューを初期化
      $('#js-sub-header-list').show();
      $('#js-sub-header-ttl').removeClass('open');
    } else {
      // サブメニューを隠す
      $('#js-sub-header-list').hide();
    }

  }, 200);
});


$(function () {
  // PC版：マウスオーバー時にサブメニューを表示
  $(".js-sp-nav-toggle").each(function () {

      if ($(window).width() > 767) {
          if ($(this).parent().is('#js-gnav-sp')) {
              return;
          }

          $(this).hover(
              function () {
                  // サブメニュー表示
                  $(this).find(".subnav-list").addClass("active");
                  // メニュー矢印を上向きに変更
                  $(this).addClass('active');
              },
              function () {
                  $(this).find(".subnav-list").removeClass("active");
                  $(this).removeClass('active');
              }
          );
      } else {
        $(this).click(
          function () {
                if ($(this).hasClass("active")) {
                    $(this).find(".subnav-list").removeClass("active");
                    // メニュー矢印を上向きに変更
                    $(this).removeClass('active');
                } else {
                    $(this).find(".subnav-list").addClass("active");
                    // メニュー矢印を上向きに変更
                    $(this).addClass('active');
                }
              }
          );
      }
  });
});


