﻿$(function () {
  // drawer
  let nav = $('.header .nav').clone();
  $('.drawer').append(nav);
  $('.menu-trigger').on('click', function () {
    $(this).toggleClass('active');
    $('.drawer').slideToggle();
    return false;
  });
  $(window).on('load resize', function () {
    let w = $(window).width();
    if (w >= 768) {
      $('.drawer').css('display', 'none');
    } else {
      if ($('.menu-trigger').hasClass('active')) {
        $('.drawer').css('display', 'block');
      }
    }
  });


 $(".pc-nav").find(".submenu").hover(function(){
			$(".pc-nav").find(".child",this).fadeIn();
	},function(){
			$(".pc-nav").find(".child",this).fadeOut();
	});



	$(".header .sp-nav .list .katsuyo").click( function (event) {
		event.preventDefault(); 
		$(".header .sp-nav .parent").toggleClass("on");
		$(".header .sp-nav .child").slideToggle();
	});


});