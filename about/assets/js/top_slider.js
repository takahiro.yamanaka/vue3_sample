const l = document.getElementById('swiperWrapper');
let nowPosition;
let hoverPosition;
let progress;
const mySwiper = new Swiper('.expSlidIn', {
  freeMode: true,
  loop: true,
  slidesPerView: 1.4,
  speed: 10000,
  autoplay: {
    delay: 0,
  },
  breakpoints: {
    1024: {
      slidesPerView: 2.5,
    }
  },
  on: {
    slideChangeTransitionStart: function(){
      l.style.transitionTimingFunction = 'linear';
    }
  },
});

const myContainer = document.getElementById('swiperContainer');
myContainer.addEventListener('mouseenter', function(e){
},false);
myContainer.addEventListener('mouseleave', function(e){

  mySwiper.once('slideChangeTransitionEnd', function(){
  });
  mySwiper.once('slideChangeTransitionStart', function(){
  });

  mySwiper.autoplay.start();
});
