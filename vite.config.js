import vue from '@vitejs/plugin-vue';
import { NodeGlobalsPolyfillPlugin } from '@esbuild-plugins/node-globals-polyfill';
import { fileURLToPath } from 'url';
import { resolve } from 'path';
const glob = require('glob');

let inputs = { src: resolve(__dirname, 'index.html') };
glob(resolve(__dirname, 'about', '**', '*.html'), function (err, files) {
  files.forEach((file) => {
    const index = Object.keys(inputs).length;
    inputs['about' + String(index)] = file;
  });
  console.log(inputs);
});

export default {
  plugins: [vue(/* options */)],
  optimizeDeps: {
    esbuildOptions: {
      define: {
        global: 'globalThis',
      },
      // Enable esbuild polyfill plugins
      // https://medium.com/@ftaioli/using-node-js-builtin-modules-with-vite-6194737c2cd2
      plugins: [
        NodeGlobalsPolyfillPlugin({
          process: true,
          buffer: true,
        }),
      ],
    },
  },
  resolve: {
    alias: {
      '~': fileURLToPath(new URL('./src', import.meta.url)),
      '@': fileURLToPath(new URL('./src', import.meta.url)),
    },
  },
  build: {
    rollupOptions: {
      input: inputs,
    },
  },
};
